﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using AutomationCourseFramework.Pages;
using AutomationCourseFramework.WebDriver;
using OpenQA.Selenium;

namespace AutomationCourseFramework.Models
{
    public class SignupRawHttp
    {
        public void Signup()
        {
            var httpClient = new HttpClient() {Timeout = TimeSpan.FromSeconds(60)};
            var requestUrl = "http://vdmitriev/fotoweb/signups/";

            var content = new StringContent("{\"firstName\":\"test5\",\"lastName\":\"test5ln\",\"email\":\"115@1.com\",\"password\":\"1\",\"termsAccepted\":true,\"userData\":[]}", Encoding.UTF8);
            content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/vnd.fotoware.signup+json");
            content.Headers.Add("X-Requested-With", "XMLHttpRequest");

            var result = httpClient.PostAsync(requestUrl, content).Result;
            var code = result.StatusCode;
            if(result.StatusCode != HttpStatusCode.NoContent)
                throw new Exception($"Cannot signup a new user. Status code is {result.StatusCode}");
        }

        public void CreateAlbum()
        {            

            LoginPage loginPage = new LoginPage();
            loginPage.LoginOrEmail.ClearAndKeyPress("vitaliy.dmitriev@fastdev.se");
            loginPage.Password.ClearAndKeyPress("1QQQqqq");
            loginPage.Login.Click();

            var cookieContainer = new CookieContainer();
            var handler = new HttpClientHandler {CookieContainer = cookieContainer};
            var _driver = DriverController.GetInstance();
            foreach (OpenQA.Selenium.Cookie cookie in _driver.Manage().Cookies.AllCookies)
            {
                cookieContainer.Add(new System.Net.Cookie(cookie.Name, cookie.Value, cookie.Path, cookie.Domain));
            }

            var httpClient = new HttpClient(handler) { Timeout = TimeSpan.FromSeconds(60) };




            var requestUrl = "http://vdmitriev/fotoweb/signups/";

            var content = new StringContent("{\"firstName\":\"test5\",\"lastName\":\"test5ln\",\"email\":\"115@1.com\",\"password\":\"1\",\"termsAccepted\":true,\"userData\":[]}", Encoding.UTF8);
            content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/vnd.fotoware.signup+json");
            content.Headers.Add("X-Requested-With", "XMLHttpRequest");

            var result = httpClient.PostAsync(requestUrl, content).Result;
            var code = result.StatusCode;
            if (result.StatusCode != HttpStatusCode.NoContent)
                throw new Exception($"Cannot signup a new user. Status code is {result.StatusCode}");
        }
    }
}

﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace AutomationCourseFramework.WebDriver
{
    public static class DriverController
    {
        private static IWebDriver _instance;
        public static IWebDriver GetInstance()
        {
            if (_instance == null)
            {
                _instance = new ChromeDriver(AppDomain.CurrentDomain.BaseDirectory);
                _instance.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(120);
                _instance.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(60);
                _instance.Manage().Window.Maximize();
                _instance.Navigate().GoToUrl("http://autotestcourse.fastdev.se");
            }

            return _instance;
        }
    }
}

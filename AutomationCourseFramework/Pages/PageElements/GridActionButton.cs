﻿using AutomationCourseFramework.Common;
using AutomationCourseFramework.Pages.Assets.AssetActions;
using OpenQA.Selenium;

namespace AutomationCourseFramework.Pages.PageElements
{
    public class GridActionButton : Button
    {
        private readonly string _rootXpath;
        private readonly AssetAction _action;
        public GridActionButton(AssetAction action) : base(BuildBaseLocator(action))
        {
            _rootXpath = $"//div[@id='center-toolbar']//div[starts-with(@class, 'js-actions-list')]//button[@data-original-title='{action.GetExtendedName()}']";
            _action = action;
        }

        private static By BuildBaseLocator(AssetAction action)
        {
            string xpath = $"//div[@id='center-toolbar']//div[starts-with(@class, 'js-actions-list')]//button[@data-original-title='{action.GetExtendedName()}']";
            return By.XPath(xpath);
        }

        public string Title => new Label(By.XPath($"{_rootXpath}/span[starts-with(@class, 'title')]")).Text;

        public void WaitForConfirmationMode()
        {
            string confirmationModeXpath = $"//div[@id='center-toolbar']//div[starts-with(@class, 'js-actions-list')]//button[@data-original-title='{_action.GetExtendedName()}' and contains(@class, 'is-confirm-mode')]";
            new Label(By.XPath(confirmationModeXpath)).WaitForElementPresent();
        }

        public void WaitForProcessIsCompleted()
        {
            string confirmationModeXpath = $"{_rootXpath}/i[starts-with(@class, 'fa') and not(contains(@class, 'fa-spinner fa-spin'))]";
            new Label(By.XPath(confirmationModeXpath)).WaitForElementPresent();
        }


    }
}

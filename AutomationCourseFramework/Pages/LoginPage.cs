﻿using AutomationCourseFramework.Pages.PageElements;
using OpenQA.Selenium;

namespace AutomationCourseFramework.Pages
{
    public class LoginPage : BasePage
    {
        public EditBox LoginOrEmail => new EditBox(By.Id("username-page"));

        public EditBox Password => new EditBox(By.Id("password-page"));
        public Button Login => new Button(By.Id("page-form-submit"));
    }
}

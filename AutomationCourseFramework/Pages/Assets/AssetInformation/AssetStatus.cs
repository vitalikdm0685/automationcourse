﻿using System;
using AutomationCourseFramework.Common;

namespace AutomationCourseFramework.Pages.Assets.AssetInformation
{
    public enum AssetStatus
    {
        [ExtendedName("Not set", "none")]
        NotSet = -1,

        [ExtendedName("None", "0")]
        None,

        [ExtendedName("High", "1")]
        High,

        [ExtendedName("2", "2")]
        Two,

        [ExtendedName("3", "3")]
        Three,

        [ExtendedName("4", "4")]
        Four,

        [ExtendedName("Normal", "5")]
        Normal,

        [ExtendedName("6", "6")]
        Six,

        [ExtendedName("7", "7")]
        Seven,

        [ExtendedName("Low", "8")]
        Low
    }

    public static class AssetStatusParcer
    {
        public static AssetStatus Parce(string value)
        {
            try
            {
                var splitedStatusStrs = value.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                var statusStr = splitedStatusStrs[splitedStatusStrs.Length - 1];

                if (statusStr == "status-" || statusStr.StartsWith("status-bar"))
                    return AssetStatus.NotSet;
                var statusOrderStr = Convert.ToInt32(statusStr.Split(new[] { "status-" }, StringSplitOptions.None)[1]);

                return (AssetStatus)statusOrderStr;
            }
            catch (Exception ex)
            {

                throw new Exception($"Couldn't parce asset status from the string '{value}'", ex);
            }
        }
    }
}

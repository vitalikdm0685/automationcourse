﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationCourseFramework.Pages;
using AutomationCourseFramework.WebDriver;
using Microsoft.VisualBasic.FileIO;
using NUnit.Framework;

namespace AutomationCourseUITest
{
    [TestFixture]
    public class TestData
    {
        [TearDown]
        public void TearDown()
        {
            //DriverController.GetInstance().Close();
        }


        [Test]
        [TestCase("defaultStandard", "1QQQqqq")]
        [TestCase("defaultPro", "1QQQqqq")]
        public void LoginUniversal(string userName, string userPassword)
        {
            var driver = DriverController.GetInstance();
            driver.Navigate().GoToUrl("http://vdmitriev/fotoweb/views/login");

            LoginPage loginPage = new LoginPage();
            loginPage.LoginOrEmail.ClearAndKeyPress(userName);
            loginPage.Password.ClearAndKeyPress(userPassword);
            loginPage.Login.Click();
        }


        [Test]
        [TestCaseSource(typeof(TestParameters), nameof(TestParameters.CsvTestCases))]
        public void LoginParameters(string userName, string userPassword)
        {
            var driver = DriverController.GetInstance();
            driver.Navigate().GoToUrl("http://vdmitriev/fotoweb/views/login");

            LoginPage loginPage = new LoginPage();
            loginPage.LoginOrEmail.ClearAndKeyPress(userName);
            loginPage.Password.ClearAndKeyPress(userPassword);
            loginPage.Login.Click();
        }

        [Test]
        [TestCaseSource(typeof(TestParameters), nameof(TestParameters.ReturnCases))]
        public int Summ(int a, int b)
        {
            return a + b;
        }


        private class  TestParameters
        {
            public static IEnumerable TestCases
            {
                get
                {
                    return new[]
                    {
                        new TestCaseData("defaultStandard", "1QQQqqq"),
                        new TestCaseData("defaultPro", "1QQQqqq"),
                    };
                }
            }

            public static IEnumerable CsvTestCases
            {
                get
                {
                    var testData = new List<TestCaseData>();
                    using (var parser = new TextFieldParser(@"E:\temp\data.csv"))
                    {
                        parser.TextFieldType = FieldType.Delimited;
                        parser.SetDelimiters(";");

                        while (!parser.EndOfData)
                        {
                            string[] fields = parser.ReadFields();

                            testData.Add(new TestCaseData(fields[0], fields[1]));
                        }
                    }

                    return testData;
                }
            }

            public static IEnumerable ReturnCases
            {
                get
                {
                    return new[]
                    {
                        new TestCaseData(1, 2).Returns(3),
                        new TestCaseData(5, 2).Returns(9).SetProperty("TestCase", 8875),
                    };
                }
            }
        }



    }
}
